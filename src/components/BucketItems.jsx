import React, { Component } from 'react';
import { Navbar, NavItem, NavDropdown, MenuItem, Nav, Button } from 'react-bootstrap'
import { Table, Modal, Form, FormControl, FormGroup } from 'react-bootstrap'
import { ToastContainer, toast } from 'react-toastify';
import 'react-toastify/dist/ReactToastify.min.css' 

var axios = require('axios');
const url = 'https://robert-bucket-lists-api.herokuapp.com/'

export default class BucketItems extends Component {
    constructor() {
        super();
        this.state = {
            items: [],
            addItem: false,
            editItem: false,
            deleteItem: false,
            currentItemId: 0
        }
        this.getItems = this.getItems.bind(this);
        this.addItem = this.addItem.bind(this);
        this.deleteItem = this.deleteItem.bind(this);
        this.editItem = this.editItem.bind(this);
    }

    isUserLoggedIn() {
        var token = localStorage.getItem('token');
        if (token === '') {
            this.setState({ loginRequired: true });
            return false;
        } else {
            this.setState({ loginRequired: false });
            return true;
        }
    }

    componentWillMount() {
    }
    componentDidMount() {
        if (this.isUserLoggedIn()) {
            this.getItems();
        }
    }

    getItems() {
        axios.get(url + 'bucketlists/' + localStorage.getItem('bucketId') + "/items", {
            headers: {
                'token': localStorage.getItem('token'),
                'Content-Type': 'application/json'
            }
        })
            .then(response => {
                
                this.setState({ items: response.data.items });
            })
            .catch(function (error) {
                console.log(error);
            });
    }

    addItem(e) {
        e.preventDefault()
        axios.post(url + 'bucketlists/' + localStorage.getItem('bucketId') + "/items",
            { 'itemname': document.getElementById('itemname').value, },
            { headers: { 'token': localStorage.getItem('token') } })
            .then(response => {
                if (response.status === 200) {
                    this.setState({ addSucces: true });
                }
                else if (response.status === 205) {
                    this.setState({ addItem: false });
                    toast("item name in use. use a different name.");
                }
            })
            .catch(function (error) {
                console.log(error);
            });
    }

    editItem(e) {
        e.preventDefault()
        axios.put(url + 'bucketlists/' + localStorage.getItem('bucketId') + "/items/" + this.state.currentItemId,
            {   
                'newname': document.getElementById('newname').value,
                'status': document.getElementById('status').value
            },
            { headers: { 'token': localStorage.getItem('token') } })
            .then(response => {
                if (response.status === 200) {
                    this.setState({ editSucees: true })
                    alert("succes")
                }
                else {
                    console.log(response.data)
                }
            })
            .catch(function (error) {
                console.log(error);
            });
    }

    deleteItem() {
        axios.delete(url + 'bucketlists/' + localStorage.getItem('bucketId') + "/items/" + this.state.currentItemId,
            { headers: { 'token': localStorage.getItem('token') } })
            .then(response => {
                if (response.status === 200) {
                    this.setState({ deleteSucces: true });
                }
                else {
                    alert(response.data);
                }
            })
            .catch(function (error) {
                console.log(error);
            });
    }
    render() {
        if (this.state.addSucces) {
            return window.location.reload();
        }
        if (this.state.deleteSucces) {
            return window.location.reload();
        }
        if (this.state.editSucees) {
            return window.location.reload();
        }
        let close = () => this.setState({ addItem: false, editItem: false, deleteItem: false });
        return (
            <div>
                <Navbar className="navbar-fixed-top">
                <div>
                {/* One container to rule them all! */}
                <ToastContainer 
                  position="top-right"
                  type="default"
                  autoClose={5000}
                  hideProgressBar={false}
                  newestOnTop={false}
                  closeOnClick
                  pauseOnHover
                />
                {/*Can be written <ToastContainer />. Props defined are the same as the default one. */}
                </div>
                    <Navbar.Header>
                        <Navbar.Brand>
                            <a href="#">Activities</a>
                        </Navbar.Brand>
                    </Navbar.Header>
                    <Nav className="navbar-right">
                        <NavItem onClick={() => this.setState({ addItem: true })}>New Item</NavItem>
                        <NavItem eventKey={2} href="#">|</NavItem>
                        <NavDropdown eventKey={3} title={"welcome " + localStorage.getItem('username')} id="basic-nav-dropdown">
                            <MenuItem eventKey={3.1}>My Profile</MenuItem>
                            <MenuItem eventKey={3.1}>Logout</MenuItem>
                        </NavDropdown>
                    </Nav>
                </Navbar>
                <div className="table-responsive container" width="90%">
                    <Table striped bordered>
                        <colgroup>
                            <col span="1" style={{ width: '55%' }} />
                            <col span="1" style={{ width: '15%' }} />
                            <col span="1" style={{ width: '15%' }} />
                            <col span="1" style={{ width: '15%' }} />
                        </colgroup>
                        <thead>
                            <tr>
                                <th>Item Name</th>
                                <th className="text-center">Status</th>
                                <th className="text-center">Edit Item</th>
                                <th className="text-center">Delete Item</th>
                            </tr>
                        </thead>
                        <tbody>
                            {this.state.items.map((item) =>
                                <tr className='clickable-row' data-href='google.com'>
                                    <td>{item.item_name}</td>
                                    <td className="text-center">{item.item_status}</td>
                                    <td className="text-center"><button type="button" className="btn btn-primary btn-sm" onClick={() => this.setState({ editItem: true, currentItem: item.item_name, currentItemId: item.item_id })}>Edit</button> </td>
                                    <td className="text-center"><button type="button" className="btn btn-primary btn-sm" onClick={() => this.setState({ deleteItem: true, currentItem: item.item_name, currentItemId: item.item_id })}>Delete</button> </td>
                                </tr>
                            )}
                        </tbody>
                    </Table>
                </div>

                {/* Add item modal */}
                <div className="modal-container" style={{ height: 200 }}>
                    <Modal
                        show={this.state.addItem}
                        onHide={close}
                        container={this}
                        aria-labelledby="add-bucket-modal-title"
                    >
                        <Modal.Header closeButton>
                            <Modal.Title id="add-bucket-modal-title">New Bucket-List Item</Modal.Title>
                        </Modal.Header>
                        <Modal.Body>
                            <Form>
                                <FormGroup>
                                    <FormControl type="text" id="itemname" placeholder="itemname" required />
                                </FormGroup>
                            </Form>
                        </Modal.Body>
                        <Modal.Footer>
                            <Button onClick={this.addItem} className="btn btn-primary">Add Item</Button>
                            <Button onClick={close}>Close</Button>
                        </Modal.Footer>
                    </Modal>
                </div>

                {/* Edit item modal */}
                <div className="modal-container" style={{ height: 200 }}>
                    <Modal
                        show={this.state.editItem}
                        onHide={close}
                        container={this}
                        aria-labelledby="edit-bucket-modal-title"
                    >
                        <Modal.Header closeButton>
                            <Modal.Title id="edit-bucket-modal-title">Edit Bucket-List Item</Modal.Title>
                        </Modal.Header>
                        <Modal.Body>
                            <Form>
                                <FormGroup>
                                    <FormControl type="text" id="newname" value={this.state.currentItem} placeholder="newname" required />
                                </FormGroup>
                                <FormGroup controlId="formControlsSelect">
                                    <FormControl componentClass="select" id="status" placeholder="Status">
                                        <option value="Done">Done</option>
                                        <option value="Not Done">Not Done</option>
                                    </FormControl>
                                </FormGroup>
                            </Form>
                        </Modal.Body>
                        <Modal.Footer>
                            <Button onClick={this.editItem} className="btn btn-primary">Edit Item</Button>
                            <Button onClick={close}>Close</Button>
                        </Modal.Footer>
                    </Modal>
                </div>

                {/* Delete item modal */}
                <div className="modal-container" style={{ height: 200 }}>
                    <Modal
                        show={this.state.deleteItem}
                        onHide={close}
                        container={this}
                        aria-labelledby="delete-bucket-modal-title"
                    >
                        <Modal.Header closeButton>
                            <Modal.Title id="add-bucket-modal-title">Delete a Bucket-List</Modal.Title>
                        </Modal.Header>
                        <Modal.Body>
                            Are you sure to delete "{this.state.currentItem}"?
                        </Modal.Body>
                        <Modal.Footer>
                            <Button onClick={this.deleteItem} className="btn btn-primary">Delete</Button>
                            <Button onClick={close}>Close</Button>
                        </Modal.Footer>
                    </Modal>
                </div>
            </div>
        );
    }
}
