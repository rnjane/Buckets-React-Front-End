import React, { Component } from 'react';
import '../bootstrap/css/bootstrap.css';
import { Row, Form, FormControl, FormGroup, Panel, Col } from 'react-bootstrap'
import { Link, Redirect } from 'react-router-dom'

var axios = require('axios');
const url = 'https://robert-bucket-lists-api.herokuapp.com/'

export default class Register extends Component {
	constructor(props) {
		super(props);
		this.state = {
			registerSucces: false
		}
		this.register = this.register.bind(this);
    }
	register(e) {
		e.preventDefault()
		axios.post(url + 'auth/register', {
			first_name: document.getElementById('first_name').value,
			last_name: document.getElementById('last_name').value,
			email: document.getElementById('email').value,
			username: document.getElementById('user_name').value,
			password: document.getElementById('password').value,
		}).then(response => {
			if (response.status === 201) {
				this.setState({ registerSucces: true });
			}
			else {
				alert(response.data);
			}
		})
			.catch(function (error) {
				console.log(error);
			});
	}
	
	render() {
		const { registerSucces } = this.state

		if (registerSucces) {
			return <Redirect to='/login' />
		}
		return (
			<Row>
				<div className="col-md-6 col-md-offset-3">
					<Panel>
						Register
							<hr />
						<Row>
							<Col className="col-lg-12">
								<Form onSubmit={this.register}>
									<FormGroup>
										<FormControl type="text" id="first_name" placeholder="First Name" required />
									</FormGroup>
									<FormGroup>
										<FormControl type="text" id="last_name" placeholder="Last Name" required />
									</FormGroup>
									<FormGroup>
										<FormControl type="text" id="user_name" placeholder="username" required />
									</FormGroup>
									<FormGroup>
										<FormControl type="text" id="email" placeholder="Email" required />
									</FormGroup>
									<FormGroup>
										<FormControl type="password" id="password" placeholder="password" required />
									</FormGroup>
									<FormGroup>
										<FormControl type="password" id="confirm-password" placeholder="confirm password" required />
									</FormGroup>
									<FormGroup>
										<FormControl type="submit" value="Register" className="btn btn-primary" />
									</FormGroup>
									<FormGroup className="text-center">
										<Link to="/login" className="text-center"> Already Registered? Login</Link>
									</FormGroup>
								</Form>
							</Col>
						</Row>
					</Panel>
				</div>
			</Row>
		);
	}
}
