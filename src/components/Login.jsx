import React, { Component } from 'react';
import { Link, Redirect } from 'react-router-dom'
import { Row, Form, FormControl, FormGroup, Panel, Col } from 'react-bootstrap'

import { ToastContainer, toast } from 'react-toastify';
import 'react-toastify/dist/ReactToastify.min.css';

var axios = require('axios');
const url = 'https://robert-bucket-lists-api.herokuapp.com/'

export default class Login extends Component {
    constructor(props) {
        super(props);
        this.state = {
            loginSucces: false
        }
        this.login = this.login.bind(this);
    }


    componentWillMount() {
    }
    componentDidMount() {
        localStorage.setItem('username', '');
        localStorage.setItem('token', '');
    }

    login(e) {
        e.preventDefault()
        axios.post(url + 'auth/login', {
            username: document.getElementById('username').value,
            password: document.getElementById('password').value,
        }).then(response => {
            if (response.status === 202) {
                this.setState({ loginSucces: true });
                localStorage.setItem('username', response.data['username']);
                localStorage.setItem('token', response.data['token']);
            }
        })
            .catch(function (error) {
                if (error.response) {
                    toast(error.response.data.message);
                }
            });
    }


    render() {
        const { loginSucces } = this.state

        if (loginSucces) {
            return (<Redirect to="/bucketlists" />)
        }

        if (loginSucces) {
            return window.location.reload();
        }
        return (
            <Row>
                <ToastContainer
                    position="top-right"
                    type="default"
                    autoClose={5000}
                    hideProgressBar={false}
                    newestOnTop={false}
                    closeOnClick
                    pauseOnHover
                />
                <div className="col-md-6 col-md-offset-3">
                    <Panel>
                        Login
                        <hr />
                        <Row>
                            <Col className="col-lg-12">
                                <Form onSubmit={this.login}>
                                    <FormGroup>
                                        <FormControl type="text" id="username" placeholder="username" required />
                                    </FormGroup>
                                    <FormGroup>
                                        <FormControl type="password" id="password" placeholder="password" required />
                                    </FormGroup>
                                    <FormGroup>
                                        <FormControl type="submit" value="Login" className="btn btn-primary" />
                                    </FormGroup>
                                    <FormGroup className="text-center">
                                        <Link to="/register" className="text-center"> No Account? Register</Link>
                                    </FormGroup>
                                </Form>
                            </Col>
                        </Row>
                    </Panel>
                </div>
            </Row>
        );
    }
}
