import React, { Component } from 'react';
import { Redirect } from 'react-router-dom'
import { Navbar, NavItem, NavDropdown, MenuItem, Nav, Button } from 'react-bootstrap'
import { Table, Modal, Form, FormControl, FormGroup } from 'react-bootstrap'
import { ToastContainer, toast } from 'react-toastify';
import 'react-toastify/dist/ReactToastify.min.css' 


var axios = require('axios');

var instance = axios.create({
    baseURL: 'https://robert-bucket-lists-api.herokuapp.com/',
    headers: {'token': localStorage.getItem('token'),
    'Content-Type': 'application/json'}
  });

export default class BucketLists extends Component {
    constructor() {
        super();
        this.state = {
            addModal: false,
            editModal: false,
            deleteModal: false,
            currentBucketId: 0,
            bucketlists: []
        }
        this.getBuckets = this.getBuckets.bind(this);
        this.addBucket = this.addBucket.bind(this);
        this.deleteBucket = this.deleteBucket.bind(this);
        this.editBucket = this.editBucket.bind(this);
    }

    isUserLoggedIn() {
        var token = localStorage.getItem('token');
        if (token === '') {
            this.setState({ loginRequired: true });
            return false;
        } else {
            this.setState({ loginRequired: false });
            return true;
        }
    }

    getBuckets() {
        
        instance.get('/bucketlists')
            .then(response => {
                if (response.status === 200) {
                    this.setState({ bucketlists: response.data.Buckets });
                }

                else if (response.status === 201) {
                    this.setState({loginRequired : true})
                }
                
            })
            .catch(function (error) {
                console.log(error);
            });
    }

    componentWillMount() {
        
    }
    componentDidMount() {  
        this.getBuckets();
        this.setState({username: localStorage.getItem('username')})
    }

    addBucket(e) {
        e.preventDefault()
        instance.post('/bucketlists',
            { 'bucketname': document.getElementById('bucketname').value, })
            .then(response => {
                if (response.status === 206) {
                    toast(response.data);
                    this.setState({ addModal: false });
                }
                else if (response.status === 201) {
                    this.setState({addSucces: true});
                    toast("Add successful");
                }
            })
            .catch(function (error) {
                console.log(error);
            });
    }

    editBucket(e) {
        e.preventDefault()
        instance.put('/bucketlists/' + this.state.currentBucketId,
            { 'newname': document.getElementById('newname').value, },
            )
            .then(response => {
                if (response.status === 200) {
                    this.setState({ editSucces: true });
                    alert('succes')
                }
                else {
                    console.log(response.data)
                }
            })
            .catch(function (error) {
                console.log(error);
            });
    }    

    deleteBucket() {
        instance.delete('/bucketlists/' + this.state.currentBucketId,
            )
            .then(response => {
                if (response.status === 200) {
                    this.setState({ deleteSucces: true });
                }
                else {
                    alert(response.data);
                }
            })
            .catch(function (error) {
                console.log(error);
            });
    }


    render() {
        const { loginRequired } = this.state
        let close = () => this.setState({ addModal: false, editModal: false, deleteModal: false });

        if (loginRequired) {
            return (<Redirect to="/login" />)
        }

        if (this.state.editSucces || this.state.deleteSucces || this.state.addSucces || this.state.addFail) {
            return window.location.reload();
        }

        if (this.state.viewItems) {
            localStorage.setItem('bucketId', this.state.currentBucketId);
            return (<Redirect to="/items" />)
        }

        return (
            <div>
                
                <Navbar className="navbar-fixed-top">
                <div>
                {/* One container to rule them all! */}
                <ToastContainer 
                  position="top-right"
                  type="default"
                  autoClose={5000}
                  hideProgressBar={false}
                  newestOnTop={false}
                  closeOnClick
                  pauseOnHover
                />
                {/*Can be written <ToastContainer />. Props defined are the same as the default one. */}
                </div>
                    <Navbar.Header>
                        <Navbar.Brand>
                            <a href="">My Bucket Lists</a>
                        </Navbar.Brand>
                    </Navbar.Header>
                    <Nav className="navbar-right">
                        <NavItem onClick={() => this.setState({ addModal: true })}>New Bucket</NavItem>
                        <NavItem eventKey={2} href="#">|</NavItem>
                        <NavDropdown eventKey={3} title={"welcome " + this.state.username} id="basic-nav-dropdown">
                            <MenuItem eventKey={3.1}>My Profile</MenuItem>
                            <MenuItem eventKey={3.1}>Logout</MenuItem>
                        </NavDropdown>
                    </Nav>
                </Navbar>
                <div className="table-responsive container" width="90%">
                    <Table striped bordered>
                        <colgroup>
                            <col span="1" style={{ width: '55%' }} />
                            <col span="1" style={{ width: '15%' }} />
                            <col span="1" style={{ width: '15%' }} />
                            <col span="1" style={{ width: '15%' }} />
                        </colgroup>
                        <thead>
                            <tr>
                                <th>Bucket Name</th>
                                <th className="text-center">View Items</th>
                                <th className="text-center">Edit Bucket</th>
                                <th className="text-center">Delete Bucket</th>
                            </tr>
                        </thead>
                        <tbody>
                            {this.state.bucketlists.map((bucket) =>
                                <tr>
                                    <td>{bucket.bucket_name}</td>
                                    <td className="text-center"><button type="button" className="btn btn-primary btn-sm" onClick={() => this.setState({ currentBucketId: bucket.bucket_id, viewItems: true })}>View Items</button></td>
                                    <td className="text-center"><button type="button" className="btn btn-primary btn-sm" onClick={() => this.setState({ editModal: true, currentBucket: bucket.bucket_name, currentBucketId: bucket.bucket_id })}>Edit</button> </td>
                                    <td className="text-center"><button type="button" className="btn btn-primary btn-sm" onClick={() => this.setState({ deleteModal: true, currentBucket: bucket.bucket_name, currentBucketId: bucket.bucket_id })}>Delete</button></td>
                                </tr>
                            )}
                        </tbody>
                    </Table>
                </div>

                {/* Add a bucket list modal */}
                <div className="modal-container" style={{ height: 200 }}>
                    <Modal
                        show={this.state.addModal}
                        onHide={close}
                        container={this}
                        aria-labelledby="add-bucket-modal-title"
                    >
                        <Modal.Header closeButton>
                            <Modal.Title id="add-bucket-modal-title">New Bucket-List</Modal.Title>
                        </Modal.Header>
                        <Modal.Body>
                            <Form>
                                <FormControl type="text" id="bucketname" placeholder="bucketname" required />
                            </Form>
                        </Modal.Body>
                        <Modal.Footer>
                            <Button className="btn btn-primary" onClick={this.addBucket}>Add Bucket List</Button>
                            <Button onClick={close}>Close</Button>
                        </Modal.Footer>
                    </Modal>
                </div>

                {/* Edit bucketist modal */}
                <div className="modal-container" style={{ height: 200 }}>
                    <Modal
                        show={this.state.editModal}
                        onHide={close}
                        container={this}
                        aria-labelledby="edit-bucket-modal-title"
                    >
                        <Modal.Header closeButton>
                            <Modal.Title id="edit-bucket-modal-title">Edit Bucket-List</Modal.Title>
                        </Modal.Header>
                        <Modal.Body>
                            <Form>
                                <FormGroup>
                                    <FormControl type="text" id="oldname" required value={this.state.currentBucket} disabled />
                                </FormGroup>
                                <FormGroup>
                                    <FormControl type="text" id="newname" placeholder="newname" required />
                                </FormGroup>
                            </Form>
                        </Modal.Body>
                        <Modal.Footer>
                            <Button onClick={this.editBucket} className="btn btn-primary">Edit Bucket List</Button>
                            <Button onClick={close}>Close</Button>
                        </Modal.Footer>
                    </Modal>
                </div>

                {/* Delete bucketlist modal */}
                <div className="modal-container" style={{ height: 200 }}>
                    <Modal
                        show={this.state.deleteModal}
                        onHide={close}
                        container={this}
                        aria-labelledby="delete-bucket-modal-title"
                    >
                        <Modal.Header closeButton>
                            <Modal.Title id="add-bucket-modal-title">Delete a Bucket-List</Modal.Title>
                        </Modal.Header>
                        <Modal.Body>
                            Are you sure to delete "{this.state.currentBucket}"?
                        </Modal.Body>
                        <Modal.Footer>
                            <Button onClick={this.deleteBucket} className="btn btn-primary">Delete Bucket List</Button>
                            <Button onClick={close}>Close</Button>
                        </Modal.Footer>
                    </Modal>
                </div>
            </div>
        );
    }
}
